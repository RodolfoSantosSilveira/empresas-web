import React from 'react';
import { Switch, BrowserRouter } from 'react-router-dom';
import Route from './Route';
import SignIn from '../pages/SignIn';
import Dashboard from '../pages/Dashboard';
import Details from '../pages/Details';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={SignIn} />
        <Route path="/dashboard" component={Dashboard} isPrivate />
        <Route path="/details/:id" component={Details} isPrivate />
      </Switch>
    </BrowserRouter>
  );
}
