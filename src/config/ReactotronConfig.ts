import Reactotron from 'reactotron-react-js';
import { reactotronRedux } from 'reactotron-redux';
import reactotronSaga from 'reactotron-redux-saga';

declare global {
  interface Console {
    tron: any;
  }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
interface PluginConfig {
  except?: string[];
}

if (process.env.NODE_ENV === 'development') {
  const tron = Reactotron.configure()
    .use(reactotronRedux())
    .use(reactotronSaga({ except: [''] }))
    .connect();

  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  tron.clear!();

  // eslint-disable-next-line no-console
  console.tron = tron;
}
