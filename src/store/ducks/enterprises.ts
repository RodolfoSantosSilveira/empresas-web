import produce from 'immer';

export const Types = {
  ENTERPRISES_REQUEST: '@enterprises/ENTERPRISES_REQUEST',
  ENTERPRISES_SUCCESS: '@enterprises/ENTERPRISES_SUCCESS',
  ENTERPRISES_CLEAR: '@enterprises/ENTERPRISES_CLEAR',
  ENTERPRISE_REQUEST: '@enterprise/ENTERPRISE_REQUEST',
  ENTERPRISE_SUCCESS: '@enterprise/ENTERPRISE_SUCCESS',
  ENTERPRISE_EXPIRY: '@enterprises/ENTERPRISE_EXPIRY'
};

const INITIAL_STATE = {
  data: [],
  enterprise: {},
  expiry: false,
  initial: true,
  error: false,
  loading: false
};

export const Creators = {
  enterprisesRequest: (name?: string) => ({
    type: Types.ENTERPRISES_REQUEST,
    payload: { name }
  }),

  enterprisesSuccess: (result: any) => ({
    type: Types.ENTERPRISES_SUCCESS,
    payload: { result }
  }),

  enterprisesClear: () => ({
    type: Types.ENTERPRISES_CLEAR
  }),

  enterpriseRequest: (id: number) => ({
    type: Types.ENTERPRISE_REQUEST,
    payload: { id }
  }),

  enterpriseSuccess: (result: any) => ({
    type: Types.ENTERPRISE_SUCCESS,
    payload: { result }
  }),

  enterprisesRequestExpiry: () => ({
    type: Types.ENTERPRISE_EXPIRY
  })
};

export default function enterprises(state = INITIAL_STATE, action: any) {
  return produce(state, (draft: any) => {
    switch (action.type) {
      case Types.ENTERPRISES_REQUEST: {
        draft.initial = false;
        draft.error = false;
        draft.loading = true;
        draft.expiry = false;
        break;
      }
      case Types.ENTERPRISES_SUCCESS: {
        draft.data = action.payload.result;
        draft.error = false;
        draft.loading = false;
        draft.expiry = false;
        break;
      }
      case Types.ENTERPRISES_CLEAR: {
        draft.data = [];
        draft.enterprise = {};
        draft.error = false;
        draft.loading = false;
        draft.expiry = false;
        break;
      }
      case Types.ENTERPRISE_REQUEST: {
        draft.initial = false;
        draft.error = false;
        draft.loading = true;
        draft.expiry = false;
        break;
      }
      case Types.ENTERPRISE_SUCCESS: {
        draft.enterprise = action.payload.result;
        draft.error = false;
        draft.loading = false;
        draft.expiry = false;
        break;
      }
      case Types.ENTERPRISE_EXPIRY: {
        draft.error = true;
        draft.loading = false;
        draft.expiry = true;
        break;
      }
      default:
    }
  });
}
