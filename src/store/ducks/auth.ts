import produce from 'immer';

export const Types = {
  SIGN_IN_REQUEST: '@auth/SIGN_IN_REQUEST',
  SIGN_IN_SUCCESS: '@auth/SIGN_IN_SUCCESS',
  SIGN_FAILURE: '@auth/SIGN_FAILURE',
  SIGN_CLEAR: '@auth/SIGN_CLEAR',
  SIGN_OUT: '@auth/SIGN_OUT'
};

const INITIAL_STATE = {
  keys: null,
  signed: false,
  error: false,
  loading: false
};

export const Creators = {
  signInRequest: (email: string, password: string) => ({
    type: Types.SIGN_IN_REQUEST,
    payload: { email, password }
  }),

  signInSuccess: (result: any) => ({
    type: Types.SIGN_IN_SUCCESS,
    payload: { result }
  }),

  signFailure: () => ({
    type: Types.SIGN_FAILURE
  }),

  signClear: () => ({
    type: Types.SIGN_CLEAR
  }),

  signOut: () => ({
    type: Types.SIGN_OUT
  })
};

export default function auth(state = INITIAL_STATE, action: any) {
  return produce(state, (draft: any) => {
    switch (action.type) {
      case Types.SIGN_IN_REQUEST: {
        draft.loading = true;
        draft.error = false;
        break;
      }
      case Types.SIGN_IN_SUCCESS: {
        draft.keys = action.payload.result;
        draft.signed = true;
        draft.loading = false;
        draft.error = false;
        break;
      }
      case Types.SIGN_FAILURE: {
        draft.error = true;
        draft.loading = false;
        break;
      }
      case Types.SIGN_CLEAR: {
        draft.keys = null;
        draft.signed = false;
        draft.error = false;
        draft.loading = false;
        break;
      }
      case Types.SIGN_OUT: {
        draft.keys = null;
        draft.signed = false;
        break;
      }
      default:
    }
  });
}
