import { combineReducers } from 'redux';
import auth from './auth';
import enterprisesData from './enterprises';

const reducers = combineReducers({
  auth,
  enterprisesData
});

export default reducers;
