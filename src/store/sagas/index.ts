import { all, takeLatest } from 'redux-saga/effects';
import { Types as AuthTypes } from '../ducks/auth';
import { Types as EnterprisesTypes } from '../ducks/enterprises';
import { signIn, setToken } from './auth';
import { getEnterprises, showEnterprise } from './enterprises';

export default function* rootSaga(): Generator<any> {
  return yield all([
    takeLatest('persist/REHYDRATE', setToken),
    takeLatest(AuthTypes.SIGN_IN_REQUEST, signIn),
    takeLatest(EnterprisesTypes.ENTERPRISES_REQUEST, getEnterprises),
    takeLatest(EnterprisesTypes.ENTERPRISE_REQUEST, showEnterprise)
  ]);
}
