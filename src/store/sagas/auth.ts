import { call, put } from 'redux-saga/effects';
import api from '../../services/api';
import { Creators as AuthCreators } from '../ducks/auth';

export function* signIn({ payload }: any): Generator<any> {
  try {
    const { email, password } = payload;

    const response = yield call(api.post, 'users/auth/sign_in', {
      email,
      password
    });

    const { headers }: any = response;

    api.defaults.headers['access-token'] = headers['access-token'];
    api.defaults.headers.client = headers.client;
    api.defaults.headers.uid = headers.uid;

    const keys = {
      access_token: headers['access-token'],
      client: headers.client,
      uid: headers.uid
    };

    yield put(AuthCreators.signInSuccess(keys));
  } catch (err) {
    yield put(AuthCreators.signFailure());
  }
}

export function setToken({ payload }: any) {
  if (!payload) return;

  const { keys } = payload.auth;

  if (keys) {
    api.defaults.headers['access-token'] = keys.access_token;
    api.defaults.headers.client = keys.client;
    api.defaults.headers.uid = keys.uid;
  }
}
