import { call, put } from 'redux-saga/effects';
import api from '../../services/api';
import { Creators as EnterprisesCreators } from '../ducks/enterprises';

export function* getEnterprises({ payload }: any): Generator<any> {
  try {
    const { name } = payload;

    const response: any = yield call(api.get, `enterprises?name=${name}`);

    if (response) {
      const { enterprises } = response.data;

      yield put(EnterprisesCreators.enterprisesSuccess(enterprises));
    }
  } catch (err) {
    yield put(EnterprisesCreators.enterprisesRequestExpiry());
  }
}

export function* showEnterprise({ payload }: any): Generator<any> {
  try {
    const { id } = payload;

    const response: any = yield call(api.get, `enterprises/${id}`);

    if (response) {
      const { enterprise } = response.data;

      yield put(EnterprisesCreators.enterpriseSuccess(enterprise));
    }
  } catch (err) {
    yield put(EnterprisesCreators.enterprisesRequestExpiry());
  }
}
