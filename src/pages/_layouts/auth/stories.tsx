import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import AuthLayout, { AuthTypes } from '.';

export default {
  title: 'AuthLayout',
  component: AuthLayout,
  argTypes: {
    children: {
      type: 'string'
    }
  }
} as Meta;

export const Default: Story<AuthTypes> = (args) => <AuthLayout {...args} />;

Default.args = {
  children: 'Teste'
};
