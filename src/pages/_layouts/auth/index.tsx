import React from 'react';
import * as S from './styles';

export type AuthTypes = {
  children: React.ReactNode
}

const AuthLayout: React.FC<AuthTypes> = ({ children }: AuthTypes) => (
  <S.Wrapper>
    {!!children && children}
  </S.Wrapper>
);

export default AuthLayout;
