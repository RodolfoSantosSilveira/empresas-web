import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import DefaultLayout, { DefaultTypes } from '.';

export default {
  title: 'DefaultLayout',
  component: DefaultLayout,
  argTypes: {
    children: {
      type: 'string'
    }
  }
} as Meta;

export const Default: Story<DefaultTypes> = (args) => <DefaultLayout {...args} />;

Default.args = {
  children: 'Teste'
};
