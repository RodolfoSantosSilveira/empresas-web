import React from 'react';
import * as S from './styles';
import Header from '../../../components/Header';

export type DefaultTypes = {
  children: React.ReactNode
}

const DefaultLayout: React.FC<DefaultTypes> = ({ children }: DefaultTypes) => (
  <S.Wrapper>
    <Header />
    {!!children && children}
  </S.Wrapper>
);

export default DefaultLayout;
