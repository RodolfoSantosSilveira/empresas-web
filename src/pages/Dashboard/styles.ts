import styled, { css } from 'styled-components';
import media from 'styled-media-query';

type DescriptionTypes = {
  empty?: boolean;
}

export const Wrapper = styled.main`
  max-width: 1080px;
  margin: 0 auto;

  > div.card-box {
    cursor: pointer;
  }
`;

export const Content = styled.div`
  height: calc(100vh - 9.438rem);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Description = styled.p<DescriptionTypes>`
  ${({ theme, empty }) => css`
    color: ${empty ? theme.colors.greyish : theme.colors.charcoal_grey};
    font-size: ${theme.font.sizes.normal};

    ${media.lessThan('medium')`
      font-size: ${theme.font.sizes.small};
      margin: 20px;
    `}
  `}
`;
