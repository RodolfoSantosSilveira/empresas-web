import React, { useEffect, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useDispatch, useSelector, RootStateOrAny } from 'react-redux';
import * as S from './styles';
import Cards from '../../components/Cards';
import { EnterpriseTypes } from '../../types/enterprises';
import { Creators as authCreators } from '../../store/ducks/auth';

const Dashboard: React.FC = () => {
  const history = useHistory();

  const dispatch = useDispatch();

  const enterprisesData = useSelector((state: RootStateOrAny) => state.enterprisesData.data);
  const expiry = useSelector((state: RootStateOrAny) => state.enterprisesData.expiry);
  const loading = useSelector((state: RootStateOrAny) => state.enterprisesData.loading);
  const firstEntry = useSelector((state: RootStateOrAny) => state.enterprisesData.initial);

  const handleDetails = (id: number) => {
    history.push(`/details/${id}`);
  };

  const redirect = useCallback(() => {
    dispatch(authCreators.signOut());
    history.push('/');
    toast.warning('Sua sessão foi expirada, por favor realize o login novamente.');
  }, [dispatch, history]);

  useEffect(() => {
    if (expiry) redirect();
  }, [expiry, history, redirect]);

  if (firstEntry) {
    return (
      <S.Wrapper>
        <S.Content>
          <S.Description>Clique na busca para iniciar.</S.Description>
        </S.Content>
      </S.Wrapper>
    );
  }

  return (
    <S.Wrapper>
      {
        enterprisesData.length > 0 ? enterprisesData.map((item: EnterpriseTypes) => (
          <div className="card-box" key={item.id} onClick={() => handleDetails(item.id)}>
            <Cards data={item} />
          </div>
        )) : (
            <S.Content>
              {
                loading ? (<></>) : (
                  <S.Description empty>
                    Nenhuma empresa foi encontrada para a busca realizada.
                  </S.Description>
                )
              }
            </S.Content>
          )
      }
    </S.Wrapper>
  );
};

export default Dashboard;
