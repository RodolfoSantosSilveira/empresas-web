import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import { useDispatch } from 'react-redux';
import { Creators as authCreators } from '../../store/ducks/auth';
import { renderWithTheme } from '../../utils/tests/helpers';
import SignIn from '.';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  useDispatch: jest.fn(() => { })
}));

describe('SignIn component', () => {
  it('shoud render form inputs', () => {
    useDispatch.mockReturnValue(jest.fn());
    renderWithTheme(<SignIn />);

    expect(screen.getByLabelText(/email input/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/password input/i)).toBeInTheDocument();
    expect(screen.getByText(/Entrar/i)).toBeInTheDocument();
  });

  it('should login', () => {
    const dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);

    const { getByLabelText } = renderWithTheme(<SignIn />);

    const button = screen.getByText(/Entrar/i);

    fireEvent.change(getByLabelText(/email input/i), { target: { value: 'teste@contato.com' } });
    fireEvent.change(getByLabelText(/password input/i), { target: { value: '123' } });
    fireEvent.click(button);

    expect(dispatch).toHaveBeenCalledWith(authCreators.signInRequest('teste@contato.com', '123'));
  });
});
