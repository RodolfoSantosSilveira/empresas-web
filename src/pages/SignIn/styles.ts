import styled, { css } from 'styled-components';
import media from 'styled-media-query';

type InputBoxType = {
  error?: boolean;
}

type ButtonType = {
  error?: boolean;
}

type LoadingType = {
  loading?: boolean;
}

export const Wrapper = styled.main<LoadingType>`
  ${({ loading }) => css`
    &::before {
      ${loading && 'content: ""'};
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100vh;
      background: rgba(255, 255, 255, 0.6);
      z-index: 4;
    }
  `}
`;

export const LogoBox = styled.div`
  height: 69px;
  width: 285px;
  margin: 0 auto;

  ${media.lessThan('medium')`
    width: 200px;
  `}
`;

export const Logo = styled.img`
  object-fit: contain;
  height: 100%;
  width: 100%;
`;

export const Title = styled.h1`
  ${({ theme }) => css`
    color: ${theme.colors.charcoal_grey};
    font-size: ${theme.font.sizes.medium};
    text-transform: uppercase;
    font-weight: ${theme.font.bold};
    text-align: center;
    padding-top: 5rem;
    margin-right: 20px;
    margin-left: 20px;
  `}
`;

export const Description = styled.p`
  margin-top: 2rem;
  text-align: center;
  ${({ theme }) => css`
    color: ${theme.colors.charcoal_grey};
    font-size: ${theme.font.sizes.small};
    line-height: 1.6rem;
    margin-right: 10px;
    margin-left: 10px;
  `}
`;

export const FormBox = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 30px;
`;

export const InputBox = styled.div<InputBoxType>`
  width: 70%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 20px;
  ${({ theme, error }) => css`
    border-bottom: 1px solid  ${error ? theme.colors.red : '#000'};
    > svg {
      color: ${theme.colors.secondary};
      width: 30px;
      height: 30px;
      padding: 5px 0;
    }
`}
`;

export const Input = styled.input`
  width: 100%;
  background: none;
  border: none;
  padding: 5px 10px;
`;

export const Button = styled.button<ButtonType>`
  ${({ theme, error }) => css`
    margin-top: 20px;
    border: none;
    border-radius: ${theme.border.radius};
    background: ${error ? theme.colors.dark_grey : theme.colors.buttonColor};
    color: ${theme.colors.white};
    padding: 15px 20px;
    text-transform: uppercase;
    cursor: pointer;
    width: 60%;
  `}
`;

export const BoxSecurity = styled.div`
  cursor: pointer;
  height: 30px;
  width: 30px;

  > svg {
    color: rgba(0, 0, 0, 0.54);
    height: 100%;
    width: 100%;
  }
`;

export const BoxError = styled.div`
  height: 40px;
  width: 40px;

  ${({ theme }) => css`
    > svg {
      color: ${theme.colors.red};
      height: 100%;
      width: 100%;
    }
  `}
`;

export const MsgError = styled.p`
  ${({ theme }) => css`
    color: ${theme.colors.red};
    text-align: center;
    ${media.lessThan('medium')`
      margin: 0 20px;
    `}
  `}
`;
