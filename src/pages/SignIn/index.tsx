import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector, RootStateOrAny } from 'react-redux';
import { AiOutlineMail, AiOutlineUnlock, AiFillEye, AiOutlineEye } from 'react-icons/ai';
import { IoAlertCircleSharp } from 'react-icons/io5';
import { css } from '@emotion/react';
import ClipLoader from 'react-spinners/ClipLoader';
import * as S from './styles';
import logo from '../../assets/image/logo-home.png';
import { Creators as authCreators } from '../../store/ducks/auth';
import { Creators as CreatoresEnterprise } from '../../store/ducks/enterprises';

const override = css`
  position: absolute;
  right: 40%;
  z-index: 5;

  @media (min-width: 640px) {
    right: 46%;
  }
`;

const SignIn: React.FC = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showPass, setShowPass] = useState(true);

  const dispatch = useDispatch();
  const history = useHistory();

  const authError = useSelector((state: RootStateOrAny) => state.auth.error);
  const loading = useSelector((state: RootStateOrAny) => state.auth.loading);
  const signed = useSelector((state: RootStateOrAny) => state.auth.signed);

  const handleSubmit = () => {
    dispatch(authCreators.signInRequest(email, password));
  };

  const togglePassword = () => {
    setShowPass((prevState) => !prevState);
  };

  useEffect(() => {
    dispatch(authCreators.signClear());
  }, [dispatch, email, password]);

  useEffect(() => {
    if (signed) history.push('/dashboard');
  }, [history, signed]);

  useEffect(() => {
    dispatch(CreatoresEnterprise.enterprisesClear());
  }, [dispatch]);

  return (
    <S.Wrapper loading={loading}>
      <S.LogoBox>
        <S.Logo src={logo} alt="ioasys" />
      </S.LogoBox>
      <S.Title>Bem-vindo ao empresas</S.Title>
      <S.Description>
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
      </S.Description>
      <ClipLoader color="#57bbbc" loading={loading} css={override} size={100} />
      <S.FormBox>
        <S.InputBox error={authError}>
          <AiOutlineMail />
          <S.Input aria-label="email input" type="email" placeholder="E-mail" onChange={(e) => setEmail(e.target.value)} />
          {
            authError && (
              <S.BoxError>
                <IoAlertCircleSharp />
              </S.BoxError>
            )
          }
        </S.InputBox>
        <S.InputBox error={authError}>
          <AiOutlineUnlock />
          <S.Input aria-label="password input" type={showPass ? 'password' : 'text'} placeholder="Senha" onChange={(e) => setPassword(e.target.value)} />
          {
            authError && (
              <S.BoxError>
                <IoAlertCircleSharp />
              </S.BoxError>
            )
          }
          {
            password && authError === false && (
              <S.BoxSecurity onClick={togglePassword}>
                {showPass ? <AiOutlineEye /> : <AiFillEye />}
              </S.BoxSecurity>
            )
          }
        </S.InputBox>
        {
          authError && (
            <S.MsgError>Credenciais informadas são inválidas, tente novamente.</S.MsgError>
          )
        }
        <S.Button error={authError} type="button" onClick={handleSubmit}>
          Entrar
        </S.Button>
      </S.FormBox>
    </S.Wrapper>
  );
};

export default SignIn;
