import React from 'react';
import { useDispatch } from 'react-redux';
import { Creators as CreatoresEnterprise } from '../../store/ducks/enterprises';
import { renderWithTheme } from '../../utils/tests/helpers';
import Details from '.';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  useDispatch: jest.fn(() => { })
}));

jest.mock('react-router-dom', () => ({
  useParams: () => ({
    id: 1
  })
}));

describe('Details component', () => {
  it('should get one enterprise', () => {
    const dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);

    renderWithTheme(<Details />);

    expect(dispatch).toHaveBeenCalledWith(CreatoresEnterprise.enterpriseRequest(1));
  });
});
