import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector, RootStateOrAny } from 'react-redux';
import { Creators as CreatoresEnterprise } from '../../store/ducks/enterprises';
import * as S from './styles';
import logomarca from '../../assets/image/img-e-1-lista.png';

const Details: React.FC = () => {
  const { id }: any = useParams();
  const dispatch = useDispatch();

  const enterprise = useSelector((state: RootStateOrAny) => state.enterprisesData.enterprise);

  useEffect(() => {
    (() => {
      dispatch(CreatoresEnterprise.enterpriseRequest(id));
    })();
  }, [dispatch, id]);

  return (
    <S.Wrapper>
      <S.Card>
        {
          !!enterprise && (
            <>
              <S.LogoBox>
                <S.Logo src={logomarca || enterprise.photo} alt="logomarca" />
              </S.LogoBox>
              <S.Description>
                {enterprise.description}
              </S.Description>
            </>
          )
        }
      </S.Card>
    </S.Wrapper>
  );
};

export default Details;
