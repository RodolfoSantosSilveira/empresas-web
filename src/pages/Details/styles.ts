import styled, { css } from 'styled-components';
import media from 'styled-media-query';

export const Wrapper = styled.main`
  max-width: 1080px;
  margin: 0 auto;
`;

export const Card = styled.div`
  ${({ theme }) => css`
    background: ${theme.colors.white};
    padding: 20px 40px;
    margin-top: 40px;

    ${media.lessThan('medium')`
      margin: 40px 20px;
    `}

    ${media.between('medium', 'large')`
      margin: 40px 40px;
    `}
  `}
`;

export const LogoBox = styled.div`
  height: 160px;
  width: 100%;
`;

export const Logo = styled.img`
  object-fit: contain;
  height: 100%;
  width: 100%;
`;

export const Description = styled.p`
  margin-top: 20px;
  ${({ theme }) => css`
    color: ${theme.colors.warm_grey};
    line-height: 1.6rem;
  `}
`;
