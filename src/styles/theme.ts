export default {
  border: {
    radius: '3.6px'
  },
  font: {
    family:
      "'Roboto', -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
    light: 300,
    normal: 400,
    bold: 600,
    sizes: {
      small: '1.125rem',
      medium: '1.5rem',
      normal: '2rem',
      large: '2.125rem'
    }
  },
  colors: {
    primary: '#ebe9d7',
    secondary: '#ee4c77',
    buttonColor: ' #57bbbc',
    white: '#FFFF',
    charcoal_grey: '#383743',
    warm_grey: '#8d8c8c',
    dark_indigo: '#1a0e49',
    medium_pink: '#ee4c77',
    night_blue: '#0d0430',
    greyish: '#b5b4b4',
    red: '#ff0f44',
    dark_grey: '#748383'
  }
};
