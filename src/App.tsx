import './config/ReactotronConfig';
import React from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Router } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import Routes from './routes';
import history from './services/history';
import GlobalStyle from './styles/global';
import theme from './styles/theme';
import { store, persistor } from './store';

const App: React.FC = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <ThemeProvider theme={theme}>
        <Router history={history}>
          <ToastContainer autoClose={4000} />
          <GlobalStyle />
          <Routes />
        </Router>
      </ThemeProvider>
    </PersistGate>
  </Provider>
);

export default App;
