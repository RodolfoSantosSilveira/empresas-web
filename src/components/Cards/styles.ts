import styled, { css } from 'styled-components';
import media from 'styled-media-query';

export const Wrapper = styled.div`
  ${({ theme }) => css`
    background: ${theme.colors.white};
    width: 100%;
    padding: 20px;
    margin: 40px 0;
    display: flex;
    justify-content: space-between:
    align-items: center;

    ${media.lessThan('medium')`
      margin: 40px auto;
      width: 90%;
    `}
  `}
`;

export const LogoBox = styled.div`
  height: 160px;
  width: 293px;

  ${media.lessThan('medium')`
    width: 200px;
  `}
`;

export const Logo = styled.img`
  object-fit: contain;
  height: 100%;
  width: 100%;
`;

export const InformationBox = styled.div`
  margin: 20px;

  ${media.lessThan('medium')`
    margin: 10px;
  `}
`;

export const Title = styled.h2`
  ${({ theme }) => css`
    color: ${theme.colors.dark_indigo};
    font-size: ${theme.font.sizes.large};
    font-weight: ${theme.font.bold};
    text-transform: capitalize;

    ${media.lessThan('medium')`
      font-size: ${theme.font.sizes.medium};
    `}
  `}
`;

export const Category = styled.p`
  ${({ theme }) => css`
    color: ${theme.colors.warm_grey};
    font-size: ${theme.font.sizes.medium};
    font-weight: ${theme.font.normal};
    text-transform: capitalize;
    margin-top: 10px;

    ${media.lessThan('medium')`
      font-size: ${theme.font.sizes.small};
    `}
  `}
`;

export const Location = styled.p`
  ${({ theme }) => css`
    color: ${theme.colors.warm_grey};
    font-size: ${theme.font.sizes.small};
    font-weight: ${theme.font.normal};
    text-transform: capitalize;
    margin-top: 10px;
  `}
`;
