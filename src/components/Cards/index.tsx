import React from 'react';
import * as S from './styles';
import { EnterpriseTypes } from '../../types/enterprises';
import logomarca from '../../assets/image/img-e-1-lista.png';

interface Props {
  data: EnterpriseTypes
}

const Cards: React.FC<Props> = ({ data }: Props) => (
  <S.Wrapper>
    <S.LogoBox>
      <S.Logo src={logomarca || data.photo} alt="logomarca" />
    </S.LogoBox>
    <S.InformationBox>
      <S.Title>{data.enterprise_name}</S.Title>
      <S.Category>{data.enterprise_type.enterprise_type_name}</S.Category>
      <S.Location>{data.country}</S.Location>
    </S.InformationBox>
  </S.Wrapper>
);

export default Cards;
