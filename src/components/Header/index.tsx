import React, { useState, useEffect, useCallback } from 'react';
import { AiOutlineSearch, AiOutlineClose, AiOutlineArrowLeft } from 'react-icons/ai';
import { useDispatch, useSelector, RootStateOrAny } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import { Creators as CreatorsEnterprises } from '../../store/ducks/enterprises';
import * as S from './styles';
import logo from '../../assets/image/logo-nav.png';

const Header: React.FC = () => {
  const [modoSearch, setModoSearch] = useState(false);
  const [search, setSearch] = useState('');

  const { id }: any = useParams();

  const enterprise = useSelector((state: RootStateOrAny) => state.enterprisesData.enterprise);

  const dispatch = useDispatch();
  const history = useHistory();

  const switchSearchMode = useCallback(() => {
    setModoSearch(true);
    dispatch(CreatorsEnterprises.enterprisesRequest(search));
  }, [dispatch, search]);

  useEffect(() => {
    if (search) switchSearchMode();
  }, [search, switchSearchMode]);

  const goBack = () => {
    history.push('/');
  };

  if (id) {
    return (
      <S.WrapperDetail>
        <S.Box onClick={goBack}>
          <S.DetailWrapperSvg>
            <AiOutlineArrowLeft />
          </S.DetailWrapperSvg>
          <S.TitleEmploye>{enterprise.enterprise_name}</S.TitleEmploye>
        </S.Box>
      </S.WrapperDetail>
    );
  }

  return (
    <>
      {
        modoSearch ? (
          <S.WrapperInput>
            <S.InputBox>
              <S.SvgInputBox>
                <AiOutlineSearch />
              </S.SvgInputBox>
              <S.Input type="text" placeholder="Pesquisar" onChange={(e) => setSearch(e.target.value)} />
              <S.SvgInputBox onClick={() => setModoSearch(false)}>
                <AiOutlineClose />
              </S.SvgInputBox>
            </S.InputBox>
          </S.WrapperInput>
        ) : (
          <S.Wrapper>
            <S.LogoBox>
              <S.Logo src={logo} alt="ioasys" />
            </S.LogoBox>
            <S.SvgBox onClick={() => switchSearchMode()}>
              <AiOutlineSearch />
            </S.SvgBox>
          </S.Wrapper>
        )
      }
    </>
  );
};

export default Header;
