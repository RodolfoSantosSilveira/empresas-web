import styled, { css } from 'styled-components';
import media from 'styled-media-query';

export const Wrapper = styled.header`
  ${({ theme }) => css`
    position: relative;
    background: ${theme.colors.secondary};
    height: 9.438rem;
    display: flex;
    align-items: center;
    background-image: linear-gradient(180deg, ${theme.colors.medium_pink} 24%, ${theme.colors.night_blue} 280%);
  `}
`;

export const WrapperInput = styled.header`
  ${({ theme }) => css`
    background: ${theme.colors.secondary};
    height: 9.438rem;
    display: flex;
    align-items: center;
    justify-content: center;
    background-image: linear-gradient(180deg, ${theme.colors.medium_pink} 24%, ${theme.colors.night_blue} 280%);
  `}
`;

export const SvgBox = styled.div`
  height: 46px;
  width: 46px;
  position: absolute;
  right: 60px;
  top: 46px;
  cursor: pointer;
  ${media.lessThan('medium')`
    height: 36px;
    width: 36px;
    top: 57px;
    right: 25px;
  `}

  ${media.between('medium', 'large')`
    height: 46px;
    width: 46px;
  `}
  ${({ theme }) => css`
    > svg {
      color: ${theme.colors.white};
      height: 100%;
      width: 100%;
    }
  `}
`;

export const LogoBox = styled.div`
  height: 55px;
  width: 227px;
  margin: 0 auto;

  ${media.lessThan('medium')`
    margin: 0 20px;
    height: 55px;
    width: 147px;
  `}


  ${media.between('medium', 'large')`
    margin: 0 auto;
    height: 55px;
    width: 200px;
  `}
`;

export const Logo = styled.img`
  object-fit: contain;
  height: 100%;
  width: 100%;
`;

export const InputBox = styled.div`
  width: 80%;
  display: flex;
  align-items: center;
  justify-content: center;
  border-bottom: 1px solid #fff;
`;

export const SvgInputBox = styled.div`
  height: 40px;
  width: 40px;
  cursor: pointer;

  ${({ theme }) => css`
    > svg {
      color: ${theme.colors.white};
      height: 100%;
      width: 100%;
    }
  `}
`;

export const Input = styled.input`
  width: 90%;
  background: none;
  border: none;
  padding: 15px 10px;
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.large};
    color: ${theme.colors.white};

    ::placeholder,
    ::-webkit-input-placeholder {
      font-size: ${theme.font.sizes.large};
    }
    :-ms-input-placeholder {
      font-size: ${theme.font.sizes.large};
    }

    ${media.lessThan('medium')`
      font-size: ${theme.font.sizes.small};

      ::placeholder,
      ::-webkit-input-placeholder {
        font-size: ${theme.font.sizes.small};
      }
      :-ms-input-placeholder {
        font-size: ${theme.font.sizes.small};
      }
    `}
  `}
`;

export const WrapperDetail = styled.header`
  ${({ theme }) => css`
    position: relative;
    background: ${theme.colors.secondary};
    height: 9.438rem;
    display: flex;
    align-items: center;
    background-image: linear-gradient(180deg, ${theme.colors.medium_pink} 24%, ${theme.colors.night_blue} 280%);
  `}
`;

export const TitleEmploye = styled.h1`
  ${({ theme }) => css`
      color: ${theme.colors.white};
      margin-left: 20px;
      font-size: ${theme.font.sizes.large};

      ${media.lessThan('medium')`
        font-size: ${theme.font.sizes.medium};
      `}
  `}
`;

export const Box = styled.div`
  display: flex;
  align-items: center;
  margin-left: 60px;
  cursor: pointer;
  ${media.lessThan('medium')`
    margin-left: 30px;
  `}
`;

export const DetailWrapperSvg = styled.div`
  height: 40px;
  width: 40px;

  ${media.lessThan('medium')`
    height: 30px;
    width: 30px;
  `}

  ${({ theme }) => css`
    > svg {
      color: ${theme.colors.white};
      height: 100%;
      width: 100%;
    }
  `}
`;
