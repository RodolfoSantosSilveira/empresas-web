type EnterpriseTypePros = {
  id: number
  enterprise_type_name: string
}

export type EnterpriseTypes = {
  id: number
  email_enterprise: string
  facebook: any
  twitter: any
  linkedin: any
  phone: any
  own_enterprise: boolean
  enterprise_name: string
  photo: string
  description: string
  city: string
  country: string
  value: number
  share_price: number
  enterprise_type: EnterpriseTypePros
}
